const express = require("express");
const bodyParser = require("body-parser");
const Sequelize = require("sequelize");

const NODE_PORT = process.env.NODE_PORT || 3001;

const server = express();

server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());

server.use(express.static(__dirname + "/../client/"));

const connection = new Sequelize(
    'employees',
    'root',
    '',
    {
        host: 'localhost',
        port: 3306,
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);
const Employees = require("./models/employees")(connection, Sequelize);

const EMPLOYEES_API = "/api/employees";

// Methods

// Add new employee
server.post(
    EMPLOYEES_API,
    (req, res) => {
        console.log("Adding one employee:" + JSON.stringify(req.body));
        const employee = req.body;
        // delete employee.emp_no;
        // const whereClause = {
        //     where: { emp_no: employee.emp_no },
        //     defaults: employee
        // };
        Employees.create(employee).then((result)=>{
            console.log("Submitting data:");
            res.status(200).json(result);
        }).catch((error)=>{console.log(error);
            res.status(500).json(error)});
    }
);

// Update one employee
server.put(
    EMPLOYEES_API,
    (req, res) => {
        console.log("Updating one employee: " + JSON.stringify(req.body));
        const emp_no = req.body.emp_no;
        console.log(emp_no);
        const whereClause = { limit: 1, where: { emp_no: emp_no } };
        Employees.findOne(whereClause).then((result)=>{
            result.update(req.body);
            res.status(200).json(result);
        }).catch((error)=>{

            res.status(500).json(error);
        });
    }
);


// Get ALL
server.get(
    EMPLOYEES_API,
    (req, res) => {
        console.log("Searching > " + req.query.keyword);
        const keyword = req.query.keyword;
        const itemsPerPage = parseInt(req.query.itemsPerPage);
        const currentPage = parseInt(req.query.currentPage);
        const offset = (currentPage - 1) * itemsPerPage;
        console.log(keyword);
        console.log(itemsPerPage);
        console.log(currentPage);
        console.log(typeof offset);
        console.log(typeof keyword);
        const whereClause = {
            offset: offset,
            limit: itemsPerPage,
            where: {
                first_name: keyword
            }
        };
        Employees.findAndCountAll(whereClause).then((results)=>{
            res.status(200).json(results);
        }).catch((error)=>{
            res.status(500).json(error);
        })
    }
);

//Get ONE
server.get(
    EMPLOYEES_API+"/:emp_no",
    (req, res) => {
        console.log(req.params.emp_no);
        const emp_no = req.params.emp_no;
        console.log(emp_no);
        const whereClause = {limit: 1, where: {emp_no: emp_no}};
        Employees.findOne(whereClause).then((result)=>{
            console.log(result);
            res.status(200).json(result);
        }).catch((error)=>{
            res.status(500).json(error);
        });
    }
);

//Delete
server.delete(
    EMPLOYEES_API+"/:emp_no",
    (req, res) => { 
        console.log(req.params.emp_no);
        const emp_no = req.params.emp_no;
        console.log(emp_no);
        const whereClause = { limit: 1, where: { emp_no: emp_no } };
        Employees.findOne(whereClause).then((result)=>{
            result.destroy();
            res.status(200).json({});
        }).catch((error)=>{
            console.log("Deletion unsuccessful:", error);
            res.staus(410).json(error);
        });
    }
);

server.listen(NODE_PORT, () => {
  console.log(
    "server/server.js: express server is now running locally on port: " +
      NODE_PORT
  );
});
